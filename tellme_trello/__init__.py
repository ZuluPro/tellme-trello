"""Trello plugin-in for django-tellme"""
VERSION = (0, 1)
__version__ = '.'.join([str(i) for i in VERSION])
__author__ = 'Anthony Monthe (ZuluPro)'
__email__ = 'anthony.monthe@gmail.com'
__license__ = 'BSD'
__url__ = 'https://gitlab.com/ZuluPro/tellme-trello'
